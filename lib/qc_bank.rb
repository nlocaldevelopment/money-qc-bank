require 'her'
require 'her/paginate'
require 'request_store'
require 'qc_bank/configuration'
require 'qc_bank/version'

module QcBank
    class << self
      attr_accessor :configuration
    end

    def self.configuration
      @configuration ||= Configuration.new
    end

    def self.reset
      @configuration = Configuration.new
    end

    def self.configure
      yield(configuration)
    end
end

Dir[File.expand_path("../qc_bank/**/*.rb", __FILE__)].each{ |f| require f}
Dir[File.expand_path("../money/**/*.rb", __FILE__)].each{ |f| require f}
