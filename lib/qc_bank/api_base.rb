module QcBank
  class ApiBase
     include ::Her::Model
     include ::Her::Paginate::Collection
     uses_api QcBank.configuration.api

     parse_root_in_json true, format: :active_model_serializers

     def becomes(klass)
       klass.new.tap do |became|
         became.instance_variable_set("@attributes", @attributes)
         became.instance_variable_set("@attributes_cache", @attributes_cache)
         became.instance_variable_set("@new_record", new_record?)
       end
     end
  end

end
