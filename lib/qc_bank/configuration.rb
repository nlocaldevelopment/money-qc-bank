require_relative './middleware/authentication'
require_relative './middleware/oauth'
require 'faraday'

module QcBank
    class Configuration
      attr_accessor :default_user, :default_password, :reporting_config, :bank_host, :auth_host, :version, :vendor, :reference_coin

      def initialize
        @default_user = (::RequestStore.store[:qc_bank_user] = ENV["QC_BANK_USER"])
        @default_password = (::RequestStore.store[:qc_bank_password] = ENV["QC_BANK_PASSWORD"])
        @bank_host= ENV["QC_BANK_HOST"]
        @auth_host= ENV["QC_BANK_AUTH_URL"]
        @vendor = ENV["QC_BANK_API_VENDOR"]
        @version = ENV["QC_BANK_API_VERSION"]
        @reference_coin = ENV["QC_BANK_REFERENCE_COIN"]
        apis_init
        oauth_init
      end

      def apis_init(host=bank_host, &block)
        @api = Her::API.new
        @api.setup :url => "#{host}/" do |c|
          # Request
          c.use Faraday::Request::Retry
          c.use QcBank::Middleware::Authentication, vendor: vendor, version: version
          c.use Faraday::Request::UrlEncoded

          # Response
          c.use Her::Paginate::PaginationParser
          c.use Her::Middleware::DefaultParseJSON

          # Adapter
          c.adapter :net_http

          # allow for customizing faraday connection
          yield c if block_given?


          # inject default adapter unless in test mode
          # c.adapter Faraday.default_adapter unless c.builder.handlers.include?(Faraday::Adapter::Test)
        end
      end

      def oauth_init(host=auth_host, &block)
        @oauth = Her::API.new

        @oauth.setup :url => "#{host}/oauth" do |c|
          # Request
          c.use QcBank::Middleware::Oauth
          c.use Faraday::Request::UrlEncoded

          # Response
          c.use Her::Paginate::PaginationParser
          c.use Her::Middleware::DefaultParseJSON

          # Adapter
          c.adapter :net_http

          # allow for customizing faraday connection
          yield c if block_given?

          # inject default adapter unless in test mode
          # c.adapter Faraday.default_adapter unless c.builder.handlers.include?(Faraday::Adapter::Test)
        end
      end

      def api
       # raise exception if somehow model classes gets required
       # before the API is configured
        raise ClientNotConfigured.new("QcBank-Api") unless @api
        @api
      end

      def oauth
        # raise exception if somehow model classes gets required
        # before the API is configured
         raise ClientNotConfigured.new("QcBank-Oauth") unless @oauth
         @oauth
      end

      class ClientNotConfigured < Exception; end

    end
end
