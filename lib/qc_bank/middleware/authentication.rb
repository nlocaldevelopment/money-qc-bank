require_relative "errors"
module QcBank
  module Middleware

    class Authentication < Faraday::Middleware
      def initialize(app = nil, options = {})
        super(app)
        @options = options
        @vendor = options[:vendor] || QcBank.configuration.vendor
        @version = options[:version] || QcBank.configuration.version
      end

      def call(env)
        env[:request_headers]["Accept"] = "application/json,application/vnd.#{@vendor}-#{@version}"
        env[:request_headers]["Authorization"] = "bearer " + (::RequestStore.store[:qc_bank_token] ? ::RequestStore.store[:qc_bank_token].access_token : Token.refresh_token.access_token)
        @app.call(env).on_complete do |response_env|
          case response_env[:status]
          when 401
              ::RequestStore.store[:qc_bank_token]= nil
              raise Nlocal::Reporting::Middleware::Unauthorized, 'Invalid Credentials'
          when 406
              raise Nlocal::Reporting::Middleware::UnaceptableResponse, 'Accept header not supported'
          end
        end
      end
    end

  end
end
