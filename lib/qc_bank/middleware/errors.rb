module Nlocal
  module Reporting
    module Middleware
      class Unauthorized < Exception; end
      class UnaceptableResponse < Exception; end
    end
  end
end
