require_relative "errors"
module QcBank
  module Middleware

    class Oauth < Faraday::Middleware
      def call(env)
        @app.call(env).on_complete do |response_env|
          case response_env[:status]
          when 401
              ::RequestStore.store[:qc_bank_token] = nil
              raise Unauthorized, 'Invalid Credentials'
          end
        end
      end
    end

  end
end
