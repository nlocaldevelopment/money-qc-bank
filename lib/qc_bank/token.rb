module QcBank
    class Token
     include ::Her::Model
     uses_api QcBank.configuration.oauth
     collection_path "token"

     custom_get :info
     before_save :default_params

     def default_params
      self.email = ::RequestStore.store[:qc_bank_user] || QcBank.configuration.default_user if !self.respond_to?(:email) || !self.email
      self.password = ::RequestStore.store[:qc_bank_password] || QcBank.configuration.default_password if !self.respond_to?(:password) || !self.password
      self.grant_type = "password" if !self.respond_to?(:grant_type) || !self.grant_type
     end

     def self.refresh_token
       ::RequestStore[:qc_bank_token]= self.create
     end

    end
end
