# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'qc_bank/version'

Gem::Specification.new do |spec|
  spec.name          = "money-qc-bank"
  spec.version       = QcBank::VERSION
  spec.authors       = ["Daniel Prado"]
  spec.email         = ["daniel.prado@publicar.com"]

  spec.summary       = %q{Client for Quiero Clientes _Bank Api}
  spec.description   = %q{Client for Quiero Clientes _Bank Api}
  spec.homepage      = ""
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "bitbucket.com"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "factory_bot", "~> 4.8"
  spec.add_development_dependency "vcr","~> 3.0"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "dotenv", "~> 2.1"
  spec.add_development_dependency "byebug", "~> 9.0"
  spec.add_runtime_dependency "her", "~> 1.0"
  spec.add_runtime_dependency "request_store", "~> 1.3"
  spec.add_dependency 'money', '~> 6.10'
  spec.add_dependency 'monetize', '~> 1.7'
end
