require 'json'
require 'yaml'
require 'spec_helper'

class Money
  module Bank
    describe QcBank do

      describe "#initialize" do
        context "without &block" do
          let(:bank) {
            QcBank.new.tap do |bank|
              bank.set_rate("2018-01-24", 'USD', 'EUR', 1.33)
            end
          }

          describe "#exchange_with" do
            it "accepts str" do
              expect { bank.exchange_with("2018-01-24",Money.new(100, 'USD'), 'EUR') }.to_not raise_exception
            end

            it "accepts currency" do
              expect { bank.exchange_with("2018-01-24",Money.new(100, 'USD'), Currency.wrap('EUR')) }.to_not raise_exception
            end

            it "exchanges one currency to another" do
              expect(bank.exchange_with("2018-01-24",Money.new(100, 'USD'), 'EUR')).to eq Money.new(133, 'EUR')
            end

            it "truncates extra digits" do
              expect(bank.exchange_with("2018-01-24",Money.new(10, 'USD'), 'EUR')).to eq Money.new(13, 'EUR')
            end

            it "raises an UnknownCurrency exception when an unknown currency is requested" do
              expect { bank.exchange_with("2018-01-24",Money.new(100, 'USD'), 'BBB') }.to raise_exception(Currency::UnknownCurrency)
            end

            it "raises an UnknownRate exception when an unknown rate is requested" do
              expect { bank.exchange_with("2018-01-24",Money.new(100, 'USD'), 'MXN') }.to raise_exception(UnknownRate)
            end

            #it "rounds the exchanged result down" do
            #  bank.add_rate("USD", "EUR", 0.788332676)
            #  bank.add_rate("EUR", "YEN", 122.631477)
            #  expect(bank.exchange_with(Money.new(10_00,  "USD"), "EUR")).to eq Money.new(788, "EUR")
            #  expect(bank.exchange_with(Money.new(500_00, "EUR"), "YEN")).to eq Money.new(6131573, "YEN")
            #end

            it "accepts a custom truncation method" do
              proc = Proc.new { |n| n.ceil }
              expect(bank.exchange_with("2018-01-24",Money.new(10, 'USD'), 'EUR', &proc)).to eq Money.new(14, 'EUR')
            end

            it 'works with big numbers' do
              amount = 10**20
              expect(bank.exchange_with("2018-01-24",Money.usd(amount), :EUR)).to eq Money.eur(1.33 * amount)
            end

            it "preserves the class in the result when given a subclass of Money" do
              special_money_class = Class.new(Money)
              expect(bank.exchange_with("2018-01-24",special_money_class.new(100, 'USD'), 'EUR')).to be_a special_money_class
            end

            it "doesn't loose precision when handling larger amounts" do
              expect(bank.exchange_with("2018-01-24",Money.new(100_000_000_000_000_01, 'USD'), 'EUR')).to eq Money.new(133_000_000_000_000_01, 'EUR')
            end
          end
        end

        context "with &block" do
          let(:bank) {
            proc = Proc.new { |n| n.ceil }
            QcBank.new(&proc).tap do |bank|
              bank.set_rate("2018-01-24",'USD', 'EUR', 1.33)
            end
          }

          describe "#exchange_with" do
            it "uses the stored truncation method" do
              expect(bank.exchange_with("2018-01-24",Money.new(10, 'USD'), 'EUR')).to eq Money.new(14, 'EUR')
            end

            it "accepts a custom truncation method" do
              proc = Proc.new { |n| n.ceil + 1 }
              expect(bank.exchange_with("2018-01-24",Money.new(10, 'USD'), 'EUR', &proc)).to eq Money.new(15, 'EUR')
            end
          end
        end
      end

      context "with_data" do
        let(:bank) {
          QcBank.new.tap do |bank|
            VCR.use_cassette 'load_data' do
              bank.load_data("2018-01-24")
            end
          end
        }

          describe "#set_rate" do
          it "sets a rate" do
            bank.set_rate("2018-01-24",'USD', 'EUR', 1.25)
            expect(bank.get_rate("2018-01-24",'USD', 'EUR')).to eq 1.25
          end

          it "raises an UnknownCurrency exception when an unknown currency is passed" do
            expect { bank.set_rate("2018-01-24",'AAA', 'BBB', 1.25) }.to raise_exception(Currency::UnknownCurrency)
          end
        end

        describe "#get_rate" do
          it "returns a rate" do
            bank.set_rate("2018-01-24", 'USD', 'EUR', 1.25)
            expect(bank.get_rate("2018-01-24", 'USD', 'EUR')).to eq 1.25
          end

          it "raises an UnknownCurrency exception when an unknown currency is passed" do
            expect { bank.get_rate("2018-01-24", 'AAA', 'BBB') }.to raise_exception(Currency::UnknownCurrency)
          end
        end
      end

      describe "#export_rates" do
        before :each do
          subject.set_rate("2018-01-24", 'USD', 'EUR', 1.25)
          subject.set_rate("2018-01-24", 'USD', 'JPY', 2.55)

          @rates = { "2018-01-24"=> { "USD_TO_EUR" => 1.25, "USD_TO_JPY" => 2.55 }}
        end

        context "with format == :json" do
          it "should return rates formatted as json" do
            json = subject.export_rates(:json)
            expect(JSON.load(json)).to eq @rates
          end
        end

        context "with format == :ruby" do
          it "should return rates formatted as ruby objects" do
            expect(Marshal.load(subject.export_rates(:ruby))).to eq @rates
          end
        end

        context "with format == :yaml" do
          it "should return rates formatted as yaml" do
            yaml = subject.export_rates(:yaml)
            expect(YAML.load(yaml)).to eq @rates
          end
        end

        context "with unknown format" do
          it "raises Money::Bank::UnknownRateFormat" do
            expect { subject.export_rates(:foo)}.to raise_error UnknownRateFormat
          end
        end

        context "with :file provided" do
          it "writes rates to file" do
            f = double('IO')
            expect(File).to receive(:open).with('/tmp/rates', 'w').and_yield(f)
            expect(f).to receive(:write).with(JSON.dump(@rates))

            subject.export_rates(:json, '/tmp/rates')
          end
        end
      end

      describe "#import_rates" do
        context "with format == :json" do
          it "loads the rates provided" do
            s = '{"2018-01-24": {"USD_TO_EUR":1.25,"USD_TO_JPY":2.55}}'
            subject.import_rates(:json, s)
            expect(subject.get_rate("2018-01-24", 'USD', 'EUR')).to eq 1.25
            expect(subject.get_rate("2018-01-24", 'USD', 'JPY')).to eq 2.55
          end
        end

        context "with format == :ruby" do
          it "loads the rates provided" do
            s = Marshal.dump({"2018-01-24"=>{"USD_TO_EUR"=>1.25,"USD_TO_JPY"=>2.55}})
            subject.import_rates(:ruby, s)
            expect(subject.get_rate("2018-01-24",'USD', 'EUR')).to eq 1.25
            expect(subject.get_rate("2018-01-24",'USD', 'JPY')).to eq 2.55
          end
        end

        context "with format == :yaml" do
          it "loads the rates provided" do
            s = {"2018-01-24"=>{"USD_TO_EUR"=>1.25,"USD_TO_JPY"=>2.55}}.to_yaml
            subject.import_rates(:yaml, s)
            expect(subject.get_rate("2018-01-24",'USD', 'EUR')).to eq 1.25
            expect(subject.get_rate("2018-01-24",'USD', 'JPY')).to eq 2.55
          end
        end

        context "with unknown format" do
          it "raises Money::Bank::UnknownRateFormat" do
            expect { subject.import_rates(:foo, "")}.to raise_error UnknownRateFormat
          end
        end
      end
    end
  end
end
