require 'spec_helper'
RSpec.describe QcBank::Rate do

 context "[Admin]" do
   before :all do
     test_user
     test_password

     VCR.use_cassette('token_create_ok') do
       RequestStore.store[:qc_bank_token] = QcBank::Token.create
     end
   end

   describe :index do
     context 'rates' do
       use_vcr_cassette "rates_list"

       subject(:rates) {
         QcBank::Rate.all }

       it { expect(rates.size).to be >= 1 }
     end
   end

 end
end
