require 'spec_helper'
RSpec.describe QcBank::Token do
  before :all do
    test_user
    test_password
  end

  describe :create do
    context 'ok' do
      use_vcr_cassette "token_create_ok"
      subject{ RequestStore.store[:qc_bank_token]||= QcBank::Token.create }
      it { expect(subject.access_token).not_to be_empty }
    end
  end
end
