require "bundler/setup"
Bundler.setup
require 'dotenv'
Dotenv.load
require "money/bank/qc_bank"
require "qc_bank"


require 'factory_girl'
require 'her'
require 'vcr'
require 'byebug'

VCR.configure do |c|
  c.cassette_library_dir = 'spec/cassettes'
  c.hook_into :webmock
  c.default_cassette_options = { record: :new_episodes,
                                 erb: true,
                                 match_requests_on: [:uri, :body, :method]
                               }

  c.filter_sensitive_data('<PASSWORD>') do |interaction|
    CGI.escape(test_password)
  end
  c.filter_sensitive_data('<USER>') do |interaction|
    CGI.escape(test_user)
  end
  c.filter_sensitive_data('<DUMMY_TOKEN>') do |interaction|
    CGI.escape(RequestStore.store[:token].access_token) if RequestStore.store[:token]
  end
  c.filter_sensitive_data('<DUMMY_TOKEN>') do |interaction|
    token_matches= /"access_token":"(?<token>.+)",/.match(interaction.response.body)
    token_matches ? token_matches[:token] : nil
  end
  c.before_record do |i|
    i.response.body.force_encoding('UTF-8')
  end
  #c.allow_http_connections_when_no_cassette = true
end


RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  config.extend VCR::RSpec::Macros

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    FactoryGirl.find_definitions
  end
end

def test_user
  RequestStore.store[:qc_bank_user]||= ENV["QC_BANK_USER"]
end

def test_password
  RequestStore.store[:qc_bank_password]||= ENV["QC_BANK_PASSWORD"]
end
